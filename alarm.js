function alarm(level, n) {
  switch (level) {
    case "alert": //For main alarm
      //Animation
      document.getElementById("clock-div").classList.add("alarm-animation");
      document.getElementById("close-clock-div").classList.add("alarm-animation");

      //Audio
      var audioCtx = new AudioContext();

      var alertosc = audioCtx.createOscillator();
      var alertgain = audioCtx.createGain();



      alertosc.connect(alertgain);
      alertgain.connect(audioCtx.destination);
      alertgain.gain.value = 0.0001;
      alertosc.type = 'sine';
      alertosc.frequency.setValueAtTime(500, 0);
      alertosc.frequency.setValueAtTime(400, 0.8);

      alertosc.frequency.setValueAtTime(440, 1.6);
      alertosc.frequency.setValueAtTime(300, 2.4);


      alertosc.start()
      alertosc.stop(4);

      alertgain.gain.exponentialRampToValueAtTime(1.0, audioCtx.currentTime + 0.04);
      alertgain.gain.linearRampToValueAtTime(0.0001, audioCtx.currentTime + 0.8);

      alertgain.gain.exponentialRampToValueAtTime(1.0, audioCtx.currentTime + 0.84);
      alertgain.gain.linearRampToValueAtTime(0.0001, audioCtx.currentTime + 1.6);

      alertgain.gain.exponentialRampToValueAtTime(1.0, audioCtx.currentTime + 1.64);
      alertgain.gain.linearRampToValueAtTime(0.0001, audioCtx.currentTime + 2.4);

      alertgain.gain.exponentialRampToValueAtTime(1.0, audioCtx.currentTime + 2.44);
      alertgain.gain.linearRampToValueAtTime(0.0001, audioCtx.currentTime + 3.2);

      break;
    case "warn": //For reminders
      var audioCtx = new AudioContext();

      var warnosc = audioCtx.createOscillator();
      var warngain = audioCtx.createGain();

      warnosc.connect(warngain);
      warngain.connect(audioCtx.destination);
      warngain.gain.value = 0.0001;
      warnosc.type = 'sine';
      warnosc.frequency.setValueAtTime(500, 0);

      warnosc.start();
      warnosc.stop(1);
      warngain.gain.exponentialRampToValueAtTime( 0.5, audioCtx.currentTime + 0.04);
      warngain.gain.linearRampToValueAtTime(0.0001, audioCtx.currentTime + 0.8);

      if(n > 1) {
        setTimeout(function() {alarm("warn", n-1)}, 1000);
      }
      break;
  }
}
