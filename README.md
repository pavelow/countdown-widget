# Countdown-widget

Web based countdown timer which can be set via URL arguments.

## Usage
Time values are passed in `hh:mm:ss` format, leading zeros can be truncated, negative values accepted.

User must to click "Start" button because /some/ browsers don't allow audio to play without prior user interaction.

Because of how the clock synchronisation works, to use the absolute timer mode the page must be accessed via a http server.
i.e. `python3 -m http.server`.

### Countdown
Timer will go off after a set period of time.

Example: `index.html?c=10:0` or `index.html?c=600`
will sound an alarm after 10 minutes.


### Absolute Countdown
Timer will count down to an absolute time.

Example: `index.html?t=2020-08-10T14:00:00+10:00`
will count down to 2:00:00PM at UTC+10Hrs on the 10th of August 2020.


### Reminders
Reminders can be set to go off at a time before the alarm goes off.

Example: `index.html?c=10:0&r=0:1:0-1/0:0:30-2`
will trigger a 10 minute timer with a reminder at 1 minute that will chime once and another at 30 seconds from the alarm which will chime twice.


## TODO
* Make things prettier?
* Look for bugs.
