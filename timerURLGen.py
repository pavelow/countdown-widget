from datetime import datetime, timezone, timedelta

class TimerURLGenerator():
    def __init__(self, baseURL):
        self.baseURL = baseURL;
        self.reminders = {};

    def addReminder(self, timeRemaining, nChimes):
        self.reminders[int(timeRemaining.total_seconds())] = nChimes;

    def clearReminders(self):
        self.reminders = {};

    def getRemindersArg(self):
        url = "r="
        for i in self.reminders:
            url = url + ("{0}-{1}/".format(i, self.reminders[i]))
        return url

    def getAbsoluteTimeURL(self, absDatetime):
        return "{0}?t={1}&{2}".format(self.baseURL, absDatetime.isoformat(), self.getRemindersArg())

    def getRelativeTimeURL(self, relTime):
        return "{0}?c={1}{2}".format(self.baseURL, (int(relTime.total_seconds())), self.getRemindersArg())

if __name__ == "__main__":
    # Usage Example:

    absTime = datetime(2020, 8, 11, 14, 11, tzinfo=timezone(timedelta(hours=10)))
    relTime = timedelta(minutes=10)

    reminderTime = timedelta(minutes=5)

    urlGen = TimerURLGenerator("index.html") # Initialise with base URL

    urlGen.addReminder(reminderTime, 1) # Add reminder which will chime once at reminderTime before the timer goes off

    # Generate URLs
    print(urlGen.getRelativeTimeURL(relTime))
    print(urlGen.getAbsoluteTimeURL(absTime))
