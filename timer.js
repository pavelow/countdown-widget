function init() {
  document.addEventListener("wheel", function(event){
    if(event.target.type === "number"){
        if(event.deltaY < 0) {
          if(event.target.value == 59) {
            if(event.target.classList.contains("h")) {
              event.target.value++;
            } else {
              event.target.value = 0;
            }
          } else {
            event.target.value++;
          }
        } else if(event.target.value > 0) {
          event.target.value--;
        } else {
          if(event.target.classList.contains("h")) {
            event.target.value = 0;
          } else {
            event.target.value = 59;
          }
        }
    }

    if(event.target.type === "time"){
        if(event.deltaY < 0) {
          event.target.stepUp();
        } else {
          event.target.stepDown();
        }
    }
    getForm();
  });

  getForm(); //Getform onload
  RenderPage(parseURL()); //ParseURL onload
}

function RenderPage(args) {

  //Check if this is a timer link, if not render the url generator
  if("c" in args || "t" in args) {
    if (timeIsInPast(args)) {
      document.getElementById("error").innerHTML = "<h3 style=\"color: yellow;\">This timer seems to be set for a time in the past, please ensure you're using the correct URL.</h3>";
      document.getElementById("error").style = "display: grid;";
    } else {
      document.getElementById("btnStart").hidden = false;
      document.getElementById("urlGen").style = "display: none";
      document.getElementById("clock").style = "display: grid;";

      // Render reminders to html
      reminders = parseReminders(args);
      for(seconds in reminders) {
        var html = document.createElement("div");
        html.classList = ["hcenter reminder-div clock-grid"];
        html.id = "reminder" + seconds;
        html.innerHTML =  "Reminder at " + renderTime(seconds);
        document.getElementById("clock").insertBefore(html, document.getElementById("close-clock-div"));
      }
    }
  } else {
    document.getElementById("urlGen").style = "display: grid";
  }
}

//Parse reminders string from args
function parseReminders(args) {
  var reminders = {};
  if("r" in args) {
    r = args["r"].split("/");
    for(var i = 0; i < r.length; i++) {
      nChimes = r[i].split("-")[1];
      seconds = parseHMStoSeconds(r[i].split("-")[0].split(":"));

      if (!isNaN(seconds)) {
        if(!(seconds in reminders)) { //Only add the first instance of a specific reminder
          reminders[seconds] = nChimes;
        }
      }
    }
  }
  return reminders;
}

function timeIsInPast(args) {
  // Check if time is in the past
  var diff = 0;
  if ("t" in args) {
    update_Local_UTC_Offset(); // Set SecondsOffset
    var today = new Date();
    var diff = 0;
    if(args["t"].match(/([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9,:]){5,8}\+([0-9]{2}:[0-9]{2})/g)) {
      target = new Date(args["t"]);
      diff = Math.floor((target.getTime() - today.getTime())/1000) + parseInt(window.localStorage.getItem("SecondsOffset")); //Add offset we calculated from http header
    }
  } else if ("c" in args) {
    t = args["c"].split(":");
    diff = parseHMStoSeconds(t);
  }

  if (diff < 0) {
    return true;
  } else {
    return false;
  }
}

// Parses the URL and returns a dict of arguments
function parseURL() {
  dict = {};
  urlString = unescape(window.location.href);
  if (urlString.match(/\?.*=.*/g)) {
    args = urlString.split("?")[1].split("&");
    for(var i = 0; i < args.length; i++) {
      pair = args[i].split("=");
      dict[pair[0]] = pair[1];
    }
  }
  return dict;
}

function parseHMStoSeconds(t) {
  t.reverse();
  var seconds = -1;
  if(t.length > 0 & t.length <= 3) {
    var seconds = parseInt(t[0]);
    for(var i = 1; i < t.length; i++) {
      seconds += parseInt(t[i])*(Math.pow(60,i));
    }
  }
  return seconds;
}

function parseStringtoHMS(string) {

  t = string.split(":").map(x => parseInt(x)).reverse();
  hms = [0, 0, 0];
  if(t.length > 0 & t.length <= 3) {
    for(var i = 0; i < t.length; i++) {
      n = parseInt(t[i])
      if(!isNaN(n)) {
        hms[i] = n;
      }
    }
  }
  return hms.reverse();
}


//Initialise timer based on args
function timerInit(args) {
  document.getElementById("btnStart").style = "display: none;";

  var reminders = {}

  if("r" in args) {
    r = args["r"].split("/");
    for(var i = 0; i < r.length; i++) {
      nChimes = r[i].split("-")[1];
      seconds = parseHMStoSeconds(r[i].split("-")[0].split(":"));

      if (!isNaN(seconds)) {
        reminders[seconds] = nChimes;
      }
    }
  }

  if("c" in args) { //Assume we want a countdown timer
    //parse hh:mm:ss
    t = args["c"].split(":");
    seconds = parseHMStoSeconds(t);
    if(seconds > 0) {

      //Initialise timer
      var c = new timer(reminders);
      c.start(seconds);
    }
  } else if("t" in args) { //Assume we want to countdown to a time

    if(args["t"].match(/([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9,:]){5,8}\+([0-9]{2}:[0-9]{2})/g)) {
      target = new Date(args["t"]);

      c = new timerUTC(target, reminders);
      c.start();
    }
  } else {
    console.log("Nothing");
  }
}


// Renders seconds to a H:M:S string with no leading H if H = 0
function renderTime(seconds) {
  negative = false;
  if(seconds < 0) {
    negative = true;
    seconds *= -1;
  }

  hours = Math.floor(seconds/(60*60));
  minutes = Math.floor((seconds%(60*60))/60);
  seconds -= hours*60*60 + minutes*60

  string = "";

  string += (negative ? "-":"");
  string += (hours == 0 ? "":String(hours)+":");
  string += ("00" + String(minutes)).slice(-2)+":"+("00" + String(seconds)).slice(-2);

  return string;
}

function timer(reminders) {
  this.remaining = 0;
  this.reminders = reminders;
  this.start = function(time) {
    this.remaining = time+1;
    var self = this;
    this.timer = setInterval(function() { self.tick(); }, 1000); //Because JS is silly
    this.tick();
  };
  this.tick = function() {
    this.remaining--;
    document.getElementById("time").innerHTML = renderTime(this.remaining);


    if(this.remaining in this.reminders) {
      alarm("warn", this.reminders[this.remaining]);
      document.getElementById("reminder" + this.remaining).classList.add("remind-animation");
      delete this.reminders[this.remaining];
    }


    if(this.remaining <= 0) {
      clearInterval(this.timer)
      document.getElementById("btnReset").style = "";
      alarm("alert", 1);
    }
  };
};

function timerUTC(target, reminders) {
  this.target = target;
  this.reminders = reminders;

  this.start = function(time) {
    update_Local_UTC_Offset(); //Get absolute time offset
    var self = this;
    this.timer = setInterval(function() { self.tick(); }, 500); //Because JS is silly
    this.tick();
  };

  this.tick = function() {
    //Get UTC Time
    var today = new Date();
    //Add offset we calculated from http header
    var diff = Math.floor((target.getTime() - today.getTime())/1000) + parseInt(window.localStorage.getItem("SecondsOffset"));

    if(diff in this.reminders) {
      alarm("warn", this.reminders[diff]);
      document.getElementById("reminder" + diff).classList.add("remind-animation");
      delete this.reminders[diff];
    }

    document.getElementById("time").innerHTML = renderTime(diff);
    if(diff <= 0) {
      clearInterval(this.timer)
      alarm("alert");
    }
  };
};

//This function gets the form data and creates a url
function getForm() {
  if(document.getElementById("abs").checked) {
    document.getElementById("absolute").hidden = false;
    document.getElementById("relative").hidden = true;

    var time = document.getElementById("target-date").value;
    time +=  "T" + document.getElementById("target-time").value;
    time += "+" +  ("00" + String(document.getElementById("tzhours").value)).slice(-2)
    time += ":" + ("00" + String(document.getElementById("tzminutes").value)).slice(-2);


    document.getElementById("url").value = window.location.href + "?t=" + time; //2020-08-10T14:00:00+10:00
  }

  if(document.getElementById("rel").checked) {
    document.getElementById("absolute").hidden = true;
    document.getElementById("relative").hidden = false;

    var hrs = document.getElementById("hours").value;
    var mins = document.getElementById("minutes").value;
    var secs = document.getElementById("seconds").value;

    document.getElementById("url").value = window.location.href + "?c=" + hrs + ":" + mins + ":" + secs;
  }

  document.getElementById("url").value += reminder("urlString");

}

function copyURL() {
  var copyText = document.getElementById("url");
  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/
  document.execCommand("copy");
}

function gotoURL() {
  window.location.href = document.getElementById("url").value;
}

function reminder(action) {
  if( typeof reminder.n == 'undefined' ) {
          reminder.n = 0;
  }


  switch(action) {
    case "add":
      var html = document.createElement("div");
      html.classList = ["reminder"];
      html.id = "r" + reminder.n;
      html.innerHTML += "<label class=\"lblReminder\">Reminder @</label>";
      html.innerHTML += "<button style=\"height: 2em;\" onclick=\"reminder('del')\">x</button>";
      html.innerHTML += "<input class=\"hms h\" type=\"number\" id=\"hr" + reminder.n + "\" value=\"0\">";
      html.innerHTML += "<input class=\"hms m\" type=\"number\" id=\"mr" + reminder.n + "\" value=\"5\">";
      html.innerHTML += "<input class=\"hms s\" type=\"number\" id=\"sr" + reminder.n + "\" value=\"0\">";
      html.innerHTML += "<label class=\"lblReminder\">Chime</label>";
      html.innerHTML += "<input class=\"hms\" type=\"number\" id=\"sc" + reminder.n + "\" value=\"1\">";
      html.innerHTML += "<label class=\"lblReminder\">times.</label>";

      document.getElementById("reminders").append(html);
      reminder.n++;
      getForm();
      break;

    case "del":
      document.activeElement.parentElement.parentElement.removeChild(document.activeElement.parentElement);
      getForm();
      break;

    case "urlString":
      str = "&r=";
      for(var i = 0; i < reminder.n; i++) {
        if(document.getElementById("r" + i) != null) {
          str += document.getElementById("hr" + i).value + ":";
          str += document.getElementById("mr" + i).value + ":";
          str += document.getElementById("sr" + i).value + "-";
          str += document.getElementById("sc" + i).value + "/";
        }
      }
      return str;
  }
}


/*
This function grabs the datetime from the http response header
to use as a reference to find the time delta in case clients clock is incorrect.
I make the assumption that the server time is correct and that network latency is negligable.
For my purposes, +/- 1 second is fine.
*/

function update_Local_UTC_Offset() {
  xmlhttp = new XMLHttpRequest();
  xmlhttp.open("HEAD", document.location.href.split("?")[0] + "?aa=" +new Date().getTime(),true); //append time so we don't get cached.
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4) {
      a = new Date(xmlhttp.getResponseHeader("Date"));
      b = new Date()
      offset = b.getTime() - a.getTime();
      if(Math.abs(offset) < 1000) { //if ofset is < 1000ms, i.e. less than the granularity of http Date header, assume local time is fine.
        window.localStorage.setItem("SecondsOffset", 0);
      } else {
        window.localStorage.setItem("SecondsOffset", Math.round(offset/1000));
      }
    }
  }
  xmlhttp.send(null);
}

function reset() {
  document.location.reload();
}
